describe('PS DB', () => {
beforeEach(() => {
  cy.visit('/') //visita a página
  
});
  it('Desafio 1', () => {
    cy.get('#switch-version-select').select('Bootstrap V4 Theme') //muda o valor da combo select version para "" V4 Theme

    cy.get('.floatL.t5 > .btn').click() //clica no botão "Add Record"

    cy.get('#field-customerName').type('Teste Sicredi')            //escreve no campo CustomerName
    cy.get('#field-contactLastName').type('Teste')                 //escreve no campo ContactLastName
    cy.get('#field-contactFirstName').type('seu nome')             //escreve no campo ContactFirstName
    cy.get('#field-phone').type('51 9999 - 9999')                  //escreve no campo Phone
    cy.get('#field-addressLine1').type('Av Assis Brasil, 3970')    //escreve no campo AdressLine1
    cy.get('#field-addressLine2').type('Torre D')                  //escreve no campo AdressLine2
    cy.get('#field-city').type('Porto Alegre')                     //escreve no campo City
    cy.get('#field-state').type('RS')                              //escreve no campo State
    cy.get('#field-postalCode').type('91000 - 000')                //escreve no campo PostalCode
    cy.get('#field-country').type('Brasil')                        //escreve no campo Country
    cy.get('#field-salesRepEmployeeNumber').type('Fixter')         //escreve no campo SalesRepEmployeeNumber
    cy.get('#field-creditLimit').type('200')                       //escreve no campo CreditLimit
 
    cy.get('#form-button-save').click() //clica no botão de salvar

    cy.get('#report-success') //pega o elemento que contém a mensagem de sucesso
      .should('contain', 'Your data has been successfully stored into the database.') //confere se a mensagem está correta
  })

  it('Desafio 2', () => {
    cy.get('#switch-version-select').select('Bootstrap V4 Theme') //muda o valor da combo select version para "" V4 Theme

    cy.get('.floatL.t5 > .btn').click() //clica no botão "Add Record"

    cy.get('#field-customerName').type('Teste Sicredi Lucas')      //escreve no campo CustomerName
    cy.get('#field-contactLastName').type('Teste')                 //escreve no campo ContactLastName
    cy.get('#field-contactFirstName').type('seu nome')             //escreve no campo ContactFirstName
    cy.get('#field-phone').type('51 9999 - 9999')                  //escreve no campo Phone
    cy.get('#field-addressLine1').type('Av Assis Brasil, 3970')    //escreve no campo AdressLine1
    cy.get('#field-addressLine2').type('Torre D')                  //escreve no campo AdressLine2
    cy.get('#field-city').type('Porto Alegre')                     //escreve no campo City
    cy.get('#field-state').type('RS')                              //escreve no campo State
    cy.get('#field-postalCode').type('91000 - 000')                //escreve no campo PostalCode
    cy.get('#field-country').type('Brasil')                        //escreve no campo Country
    cy.get('#field-salesRepEmployeeNumber').type('Fixter')         //escreve no campo SalesRepEmployeeNumber
    cy.get('#field-creditLimit').type('200')                       //escreve no campo CreditLimit

    // desafio 2 

    cy.get('#save-and-go-back-button').click() // Clica no botão "Save and go back to list"

    cy.get('.alert')
      .should('contain','Your data has been successfully stored into the database') //confere se a mensagem de sucesso está correta.
  
      cy.get('[placeholder="Search CustomerName"]').type('Teste Sicredi Lucas') //pega o elemento pelo placeholder

      cy.get('.table').contains('Teste Sicredi Lucas').parent().find('.select-row').click() //pega a tabela, procura a linha que tem Teste "", localiza o elemento pai e procura por todo o elemento pai uma checkbox.
  
      cy.contains('Delete').click() //clica em Delete
  
      cy.contains('Are you sure that you want to delete this 1 item?') //localiza o texto
        .should('be.visible') //verifica se a modal foi aberta
  
      cy.get('.delete-multiple-confirmation > .modal-dialog > .modal-content > .modal-footer > .btn-danger').click() //Clica no botão de Delete
  
      cy.get('.alert')
        .should('contain', 'Your data has been successfully deleted from the database.') //verifica se a mensagem de sucesso está correta.

  });
})

