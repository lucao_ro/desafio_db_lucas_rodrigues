const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap"
  },
});
